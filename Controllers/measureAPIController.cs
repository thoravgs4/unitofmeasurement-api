using System.ComponentModel.DataAnnotations;
using Measurement12.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Measurement12.Controllers {
    
    [Route("[controller]")]
    [ApiController]
    public class MeasureApiController:ControllerBase {
        private ApiModel _apiModel;
        private ConvertUOM _convertUom;
        private readonly ILogger<MeasureApiController> _logger;
        public  MeasureApiController(ILogger<MeasureApiController> logger) {
            _logger = logger;
            _apiModel = new ApiModel();
            _convertUom =
                new ConvertUOM();
        }

        [HttpGet("List Of Dimensions")]
        public ActionResult listAllDimensionClasses() =>Ok(_apiModel.ListAllDimension());
         
        [HttpGet("List Of Quantities")]
        public ActionResult quantity() =>Ok(_apiModel.ListAllQuantity());

        [HttpPost("UnitOfMeasureGivenDimensionClass")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult GetUomForDimensionClass(postDClass data) => Ok(_apiModel.UOM_GClass(data.dimensionClassName));
        
        // Given a quantity class : and then return all the unit measurment for this particular quantity
        [HttpPost("UnitOfMeasureGivenQuantityClass")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult UnitOfMeasuremetnQuantityClass(postQClass data) => Ok(_apiModel.UnitOfMeasure_GivenQuantityType(data.quantityClassName));
        
        [HttpPost("UOMConversion")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult conversion(PostData data) => Ok(_convertUom.Conversion(data.input_value,data.FromUom,data.ToUom));
    }
}