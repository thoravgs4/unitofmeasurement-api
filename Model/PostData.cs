namespace Measurement12.Model {
    public class PostData {
        public double input_value { get; set; }
        public string FromUom { get; set; }
        public string ToUom { get; set; }
    }
}