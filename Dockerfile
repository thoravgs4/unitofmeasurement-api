﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Measurement12.csproj", "./"]
RUN dotnet restore "Measurement12.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "Measurement12.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Measurement12.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Measurement12.dll"]
